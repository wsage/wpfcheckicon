﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PresentationLayer
{
    public class MainViewModel : BaseViewModel
    {
        private Dictionary<DateTime, bool> items;

        public Dictionary<DateTime, bool> Items
        {
            get
            {
                if (items == null)
                {
                    return new Dictionary<DateTime, bool>();
                }
                return items;
            }
        }
    }
}
