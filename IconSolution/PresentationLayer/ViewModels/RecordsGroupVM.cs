﻿using PresentationLayer.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PresentationLayer.ViewModels
{
    public class RecordsGroupVM : BaseVM, INotifyCollectionChanged
    {
        IRecordsGroup group;

        public RecordsGroupVM()
            :this(new RecordsGroup())
        {
        }

        public RecordsGroupVM(IRecordsGroup group)
        {
            group.LoadRecords();
            this.group = group;            
            group.DataUpdated += new DataUpdated(group_DataUpdated);
        }

        public List<Record> Records
        {
            get
            {
                return this.group.RecordList;
            }
        }

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        public void group_DataUpdated()
        {
            if (CollectionChanged != null)
            {
                CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add));
            }
        }
    }
}
