﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PresentationLayer.Models
{
    public delegate void DataUpdated();

    public class RecordsGroup : IRecordsGroup
    {
        private List<Record> recordList = new List<Record>();

        public List<Record> RecordList
        {
            get { return recordList; }
        }


        public void LoadRecords()
        {
            var nowday = DateTime.Now;

            for (int i = 0; i <= 182; i++)
            {
                recordList.Add(new Record() { DateRecord = nowday.AddDays(-i), Mark = false });
            }
            
        }

        public void LoadRecordsAsync()
        {
            var nowday = DateTime.Now;

            for (int i = 0; i <= 182; i++)
            {
                recordList.Add(new Record() { DateRecord = nowday.AddDays(-i), Mark = true });
            }

            if (this.DataUpdated != null)
            {
                DataUpdated();
            }
        }

        public event DataUpdated DataUpdated;
    }
}
