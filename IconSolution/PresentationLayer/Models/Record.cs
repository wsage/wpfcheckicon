﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PresentationLayer.Models
{
    public class Record
    {
        public DateTime DateRecord { get; set; }
        public bool? Mark { get; set; }
    }
}
