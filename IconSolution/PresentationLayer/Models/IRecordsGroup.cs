﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PresentationLayer.Models
{
    public interface IRecordsGroup
    {
        List<Record> RecordList { get; }
        event DataUpdated DataUpdated;
        void LoadRecords();
        void LoadRecordsAsync();

    }
}
